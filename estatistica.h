
#include <string>
#include <iostream>
using namespace std;
using std::string;
class Estatistica
{
public:
	Estatistica();
	void setValor(double);
	double getValor();

	void setValores(double);
	double getValores();

	void setModa(double);
	double getModa();

	void setMedia(double);
	double getMedia();
	
	void setMediana(double);
	double getMediana();
	
private:
	double valor, valores ;
};
